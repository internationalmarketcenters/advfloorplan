(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .factory('sponsorshipLocationSelectionManager', [
      'Lockable',
      sponsorshipLocationSelectionManagerProducer
    ]);

  function sponsorshipLocationSelectionManagerProducer(Lockable) {
    /**
     * Handles the active selection of sponsorship locations.
     */
    class SponsorshipLocationSelectionManager extends Lockable {
      /**
       * Init.
       */
      constructor() {
        super();
        this._selected = null;
      }

      /**
       * Select a sponsorship location.
       */
      select(sponLoc) {
        if (!this.isLocked())
          this._selected = sponLoc;
      }

      /**
       * Toggle the selected location.
       */
      toggle(sponLoc) {
        if (this.isLocked())
          return;

        if (this.isSelected(sponLoc))
          this.clear();
        else
          this.select(sponLoc);
      }

      /**
       * Clear the selection.
       */
      clear() {
        if (this.isLocked())
          return;

        this._selected = null;
      }

      /**
       * Get the selection.
       */
      getSelection() {
        return this._selected;
      }

      /**
       * Check if the sponsorship location is selected.
       */
      isSelected(sponLoc) {
        return sponLoc === this._selected;
      }
    }

    return new SponsorshipLocationSelectionManager();
  }
})(window.angular);

