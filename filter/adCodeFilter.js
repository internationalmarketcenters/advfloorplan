(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .filter('adCodeFilter', [AdCodeFilterProducer]);

  function AdCodeFilterProducer() {
    let lastSponLocs = null;
    let lastSelected = null;

    return function(sponLocs, selectedSponLoc) {
      if (!sponLocs)
        return;

      if (!selectedSponLoc)
        return sponLocs;

      if (lastSelected !== selectedSponLoc) {
        lastSelected = selectedSponLoc;
        lastSponLocs = sponLocs.filter(sl => sl.adCode === selectedSponLoc.adCode);
      }

      return lastSponLocs;
    };
  }
})(window.angular);

