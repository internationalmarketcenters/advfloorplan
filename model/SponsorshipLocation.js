(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .factory('SponsorshipLocation', [
      'matrixHelper',
      'SponsorshipLocationTransform',
      SponsorshipLocationProducer
    ]);

  function SponsorshipLocationProducer(matrixHelper, SponsorshipLocationTransform) {

    /** A class that customize raw SponsorshipLocation data. */
    class SponsorshipLocation {
      /**
       * Init.
       * @param {Object} sponLoc - A SponsorshipLocation object as retrieved
       * from the /api/Advertising/FloorPlan/SponsorshipLocation endpoint.
       */
      constructor(sponLoc) {
        // Copy all the properties to this object.
        Object.assign(this, sponLoc);

        this.transform = matrixHelper.createSVGMatrix();
        this.radius    = 6;

        if (this.sponsorshipLocationTransforms.length === 0) {
          this.transform = this.transform.translate(this.radius + 1, this.radius + 1);
        }
        else {
          // Convert each raw transform to a SponsorshipLocationTranform, which
          // has some helper methods on it and such.
          this.sponsorshipLocationTransforms = this.sponsorshipLocationTransforms.map(slt =>
            new SponsorshipLocationTransform(slt));

          // Order is important when applying saved transforms, so ensure that
          // the transforms are sorted.
          this.sponsorshipLocationTransforms.sort((l, r) => l.transOrder - r.transOrder);

          // Reduce the transforms to a single one by multiplying them all
          // together in order.
          this.transform = this.sponsorshipLocationTransforms.reduce(
            (acc, cur) => acc.multiply(cur.transform), this.transform);
        }
      }

      /**
       * Make a copy of the SponsorshipLocation instance.
       */
      clone() {
        const momento = new SponsorshipLocation(JSON.parse(JSON.stringify(this)));
        momento.transform = matrixHelper.createSVGMatrix();

        for (let prop in this.transform) {
          momento.transform[prop] = this.transform[prop];
        }

        return momento;
      }

      /**
       * Restore the SponsorshipLocation from a cloned momento.
       */
      restore(momento) {
        Object.assign(this, JSON.parse(JSON.stringify(momento)));
        this.transform = matrixHelper.createSVGMatrix();

        for (let prop in momento.transform) {
          this.transform[prop] = momento.transform[prop];
        }

        return this;
      }
    }

    return SponsorshipLocation;
  }
})(window.angular);

