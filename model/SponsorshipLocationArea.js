(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .factory('SponsorshipLocationArea', [
      SponsorshipLocationAreaProducer
    ]);

  function SponsorshipLocationAreaProducer() {
    /** A class that customize raw sponsorship location area data. */
    class SponsorshipLocationArea {
      /**
       * Init.
       * @param {Object} sponLocArea - A booth area object, as retrieved from
       * the /api/SponsorshipLocationArea endpoint.
       */
      constructor(sponLocArea) {
        // Copy all the properties to this object.
        Object.assign(this, sponLocArea);
      }

      /**
       * This class implements Rectangle.
       */
      getWidth()  { return this.mapWidth;  }
      getHeight() { return this.mapHeight; }
    }

    return SponsorshipLocationArea;
  }
})(window.angular);

