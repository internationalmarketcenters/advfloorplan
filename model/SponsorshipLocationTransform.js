(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .factory('SponsorshipLocationTransform', [
      '$resource',
      'matrixHelper',
      SponsorshipLocationTransformProducer
    ]);

  function SponsorshipLocationTransformProducer($resource, matrixHelper) {
    /** Transform for a spon loc. */
    class SponsorshipLocationTransform {
      /**
       * Init
       */
      constructor(sponLocTrans) {
        // Copy all the properties to this object.
        Object.assign(this, sponLocTrans);

        // Convert to usable SVGMatrix object.
        this.transform = matrixHelper.createSVGMatrix(this.a, this.b, this.c, this.d, this.e, this.f);
      }
    }

    return SponsorshipLocationTransform;
  }
})(window.angular);

