module.exports = function(grunt, scripts, dest)
{
  'use strict';

  var concat =
  {
    app:
    {
      src:  scripts,
      dest: dest
    }
  };

  grunt.loadNpmTasks('grunt-contrib-concat');

  return concat;
};

