(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .directive('amcSponsorshipLocation', [
      '$parse',
      'matrixHelper',
      AmcSponsorshipLocationProducer
    ]);

  function AmcSponsorshipLocationProducer($parse, matrixHelper) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        // The raw SVG element.
        const circle = ele[0];

        // Returns a function for retrieving the sponsorship location.
        const getSponLoc = $parse(attrs.amcSponsorshipLocation).bind(this, scope);

        // Initialize the circle.
        updateLoc();

        /**
         * Update the location element (the circle).
         */
        function updateLoc() {
          const sponLoc = getSponLoc();

          if (!sponLoc) {
            return;
          }

          circle.setAttributeNS(null, 'r', sponLoc.radius);
          circle.setAttributeNS(null, 'stroke', 'black');
          circle.setAttributeNS(null, 'transform', matrixHelper.matrixToString(sponLoc.transform));
        }

        // Watch for spon loc transformation changes and update when needed.
        scope.$watch(
          () => getSponLoc() ? getSponLoc().transform: null,
          updateLoc);
      }
    };
  }
})(window.angular);

