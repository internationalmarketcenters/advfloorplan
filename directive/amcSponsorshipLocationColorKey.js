(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .component('amcSponsorshipLocationColorKey', {
      template     : `
        <div class="cpPanel">
          <div class="colorKey">
            <label for="lbSelLoc">Color Key</label>

            <ul>
              <li ng-repeat="desc in vm.itemDescriptions">
                <div class="colorBox" style="background-color: {{vm.colorKey.get(desc)}};"></div>
                <span ng-bind="desc"></span>
              </li>
            </ul>
          </div>
        </div>`,
      controller   : [
        'colorHelper',
        AmcSponsorshipLocationColorKeyCtrl
      ],
      controllerAs : 'vm',
      bindings     : {
        sponLocs: '<sponsorshipLocations'
      }
    });

  function AmcSponsorshipLocationColorKeyCtrl(colorHelper) {
    const vm = this;

    vm.$onInit          = initKey;
    vm.$onChanges       = initKey;
    vm.itemDescriptions = null;
    vm.colorKey         = null;

    // Initialize the color key data.
    function initKey() {
      if (vm.sponLocs) {
        // Unique array of all item descriptions, sorted.
        vm.itemDescriptions = Array
          .from(new Set(vm.sponLocs
            .map(loc => loc.sponsorshipItem.itemDescription)))
          .sort();

        // Lookup of item description to color.
        vm.colorKey = new Map(vm.itemDescriptions
          .map(desc => [desc, colorHelper.stringToColor(desc)]));
      }
    }
  }
})(window.angular);

