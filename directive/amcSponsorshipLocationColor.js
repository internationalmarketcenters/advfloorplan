(function(angular) {
  'use strict';

  angular.module('AMC.advFloorPlan')
    .directive('amcSponsorshipLocationColor', [
      '$parse',
      'sponsorshipLocationSelectionManager',
      'colorHelper',
      AmcSponsorshipLocationColorProducer
    ]);

  function AmcSponsorshipLocationColorProducer($parse, selMgr, colorHelper) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        // The raw SVG element.
        const circle = ele[0];

        // Returns a function for retrieving the sponsorship location.
        const getSponLoc = $parse(attrs.amcSponsorshipLocationColor).bind(this, scope);

        // Both states, used as a bit mask.
        const STATE = {
          DEFAULT:  1,
          SELECTED: 2,
          HOVERED:  4
        };

        // Colors corresponding to states.  The default color is generated using
        // the locations item description (it's a string that's hashed and
        // converted to a color).
        const COLORS = {
          [STATE.DEFAULT]  : colorHelper.stringToColor(
            getSponLoc().sponsorshipItem.itemDescription),
          [STATE.SELECTED] : '#00AEEF'
        };

        let state      = STATE.DEFAULT;
        const addState = s => state |= s;
        const remState = s => state &= ~s;
        const hasState = s => !!(state & s);

        // Initialize the color.
        updateColor();

        // Update the color based on the state.
        function updateColor() {
          let color = COLORS[STATE.DEFAULT];

          if (hasState(STATE.SELECTED)) {
            color = COLORS[STATE.SELECTED];
          }

          if (hasState(STATE.HOVERED)) {
            color = colorHelper.darken(color, 60);
          }

          circle.setAttributeNS(null, 'fill', color);
        }

        ele.on('mouseover', function onMouseOver() {
          addState(STATE.HOVERED);
          updateColor();
          scope.$apply();
        });

        ele.on('mouseout', function onMouseOut() {
          remState(STATE.HOVERED);
          updateColor();
          scope.$apply();
        });

        // Watch for selection changes.
        scope.$watch(() => selMgr.getSelection(), function(sel, oldSel) {
          if (sel === getSponLoc()) {
            addState(STATE.SELECTED);
            updateColor();
          }
          else if (oldSel === getSponLoc()) {
            remState(STATE.SELECTED);
            updateColor();
          }
        });
      }
    };
  }
})(window.angular);

