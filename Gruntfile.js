module.exports = function(grunt)
{
  'use strict';

  var scripts   = require('./grunt/scriptGarner')();
  var buildFile = __dirname + '/build/AMC.advFloorPlan.min.js';

  grunt.initConfig
  ({
    jshint:  require('./grunt/jshint')(grunt, scripts),
    uglify:  require('./grunt/uglify')(grunt, buildFile, buildFile),
    concat:  require('./grunt/concat')(grunt, scripts, buildFile),
    babel:   require('./grunt/babel')(grunt, buildFile)
  });

  grunt.registerTask('default', ['jshint', 'concat', 'babel', 'uglify']);
  grunt.registerTask('build',  ['jshint', 'concat']);
};

